import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import {AddProduct} from'./AddProduct.js'
import {ProductList} from './ProductList.js'

class App extends Component {
  constructor(props){
    super(props)
    this.state={}
    this.state.products=[]
  }
  
  componentDidMount(){
   
    this.callBackendAPI();
    
  }
  
  callBackendAPI = () => {
    var place = this;
    axios.get('https://alexandravm-alexandraionita.c9users.io:8081/get-all', {
   })
  .then(function (response) {
    
    if(response.status === 200)
      place.setState({
        products : response.data
      })
  })
  .catch(function (error) {
    console.log(error);
  })
  };
  
  
  onProductAdded=(product)=>{
    this.state.products.push(product);
    let products=this.state.products;
    this.setState({
      products:products
    })
  }
  render() {
    return (
     <React.Fragment>
     <div>
      <h1>Online shop</h1>
      <AddProduct handleAdd={this.onProductAdded}/>
      <ProductList title="Products" source={this.state.products}/>
      </div>
      </React.Fragment>
    );
  }
}

export default App;
