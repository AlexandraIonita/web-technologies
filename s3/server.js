const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    console.log(req.body.productName);
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/product/:pid' , (req,res) => {
    try{
        let product;
        for(let i=0;i<products.length;i++)
            if(products[i].id==req.params.pid)
                 product=products[i];
        if(product)
            {
            product.productName=req.body.productName;
            product.price=req.body.price;
			res.status(202).json({message : 'accepted'})
            }
        else{
			res.status(404).json({message : 'not found'})
		}
    }
    catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});

app.delete('/product',(req,res)=>{
   try{
        var gasit=0;
        for(let i=0;i<products.length;i++)
            if(req.body.productName==products[i].productName)
                { 
                    products.splice(i,1);
                    i--;
                   gasit=1;
                }
           
           if(gasit==1)     
            res.status(202).json({message : 'accepted'})
        else{
			res.status(404).json({message : 'not found'})
		}
   }    
    
   catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});
app.listen(8080, () => {
    console.log('Server started on port 8080...');
});